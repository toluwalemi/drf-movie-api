# DRF-MOVIE_API

[![pipeline status](https://gitlab.com/toluwalemi/drf-movie-api/badges/master/pipeline.svg)](https://gitlab.com/toluwalemi/drf-movie-api/commits/master)

# A movie api built with DRF, Django, and Docker using TDD
# https://drf-movie-api.herokuapp.com/api/movies/